import { Injectable } from '@angular/core';
import {ITEMS} from '../shared/items';
import {Item} from '../shared/item';

@Injectable()
export class ItemService {

  constructor() { }
  getITems(): Item[] {
    return ITEMS;
  }
  getItem(id: number) {
    return ITEMS.filter((item) => item.id === id)[0];
  }
  getFeaturedItem(id: number) {
    return ITEMS.filter((item) => item.featured)[0];
  }
}
